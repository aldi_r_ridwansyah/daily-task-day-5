<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Hongkong</name>
   <tag></tag>
   <elementGuidId>c83acd74-5e6e-41f2-b3f7-4a7c809bc18d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[@value = 'Hongkong CURA Healthcare Center']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>13af2473-95f7-41e3-b850-75ec22db9ca9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Hongkong CURA Healthcare Center</value>
      <webElementGuid>bfc64ef5-971d-43e2-a385-391f7060d9b6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
