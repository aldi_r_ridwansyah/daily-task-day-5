<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Seoul</name>
   <tag></tag>
   <elementGuidId>d04eed11-8418-4c03-99c8-ba704d395e5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[@value = 'Seoul CURA Healthcare Center']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>098bd567-a04a-455e-9fe6-2628c247353c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Seoul CURA Healthcare Center</value>
      <webElementGuid>bf22e4d1-aeac-49e2-a7f8-27b1c8029259</webElementGuid>
   </webElementProperties>
</WebElementEntity>
