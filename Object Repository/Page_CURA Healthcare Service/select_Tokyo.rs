<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Tokyo</name>
   <tag></tag>
   <elementGuidId>9948995c-3de5-4570-b7d4-32b4bf01a06f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[@value = 'Tokyo CURA Healthcare Center']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>option</value>
      <webElementGuid>022b0734-a775-4940-9b36-dd16825cc71e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Tokyo CURA Healthcare Center</value>
      <webElementGuid>6cfb37e5-7643-41da-993e-91732b747349</webElementGuid>
   </webElementProperties>
</WebElementEntity>
