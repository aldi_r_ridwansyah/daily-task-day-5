<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Apply for hospital readmission_hospital_readmission</name>
   <tag></tag>
   <elementGuidId>be8a5d47-2fab-462b-990d-edd96c31946c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#chk_hospotal_readmission</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk_hospotal_readmission']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c7632c88-610e-4ffd-9347-91963653f42f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>28cedd0d-ab7d-4d0d-a127-2f7249751b4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk_hospotal_readmission</value>
      <webElementGuid>58fa0d86-0b5f-47c1-a5a3-eecaffaf2e8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>43b2dc1a-b50a-440e-a634-bc8e562ddc51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>ad5fd440-abb1-4ec8-ba75-b38c3f28b9e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk_hospotal_readmission&quot;)</value>
      <webElementGuid>ce07891f-18f0-4da3-a091-874c87678c56</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk_hospotal_readmission']</value>
      <webElementGuid>66dca9d7-81b5-4537-8e83-3ee2e500044b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[2]/div/label/input</value>
      <webElementGuid>8a125059-0caa-4ed7-8a86-188d30b87f15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>dc4b28b4-343d-45d0-bd0d-47b99135011d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk_hospotal_readmission' and @name = 'hospital_readmission']</value>
      <webElementGuid>8c72cab4-2c66-41f1-b46f-7fc05d05a15f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
