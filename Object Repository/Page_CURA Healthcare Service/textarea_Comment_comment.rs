<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_Comment_comment</name>
   <tag></tag>
   <elementGuidId>706f47c0-7a95-4219-ae14-002c17bff459</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#txt_comment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='txt_comment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>829be335-c506-4a2b-b9ea-6afbaff3b88c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>8dd416a0-2bf7-4218-9743-5b038e4a961d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_comment</value>
      <webElementGuid>19af1095-3313-4048-9a72-d3c899c22033</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>comment</value>
      <webElementGuid>0d2fe66d-fd63-4a65-b190-7179b8c42bcc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Comment</value>
      <webElementGuid>9ee0e48b-e86f-4f10-bc1d-bd65bda31ed7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>6f342b92-5406-4505-9e23-cf7cdc54bd08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txt_comment&quot;)</value>
      <webElementGuid>76bead29-39af-4ff7-a1b4-b49238c0817e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='txt_comment']</value>
      <webElementGuid>7339f917-c3eb-45e9-b61b-78e5f68199b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[5]/div/textarea</value>
      <webElementGuid>0c5b422d-eb70-4430-9c0e-7b08f7250445</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>946fb92a-0473-4975-a6eb-4f405e2c7016</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'txt_comment' and @name = 'comment' and @placeholder = 'Comment']</value>
      <webElementGuid>ea09ee20-8d64-45de-850b-ed700a9a439a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
